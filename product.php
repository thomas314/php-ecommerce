 <?php


class Product {
    private $id;
    private $name;
    private $price;
    public function __construct ($id_i,$name_i,$price_i) {
        $this->id = $id_i;
        $this->name = $name_i;
        $this->price = $price_i;
    }
    public function get_product_as_array () {
        return  [
            "id" => $this->id, 
            "name" => $this->name, 
            "price" => $this->price];
    }
    public function get_id () {
        return $this->id;
    }
    public function get_name () {
        return $this->name;
    }
    public function get_price () {
        return $this->price;
    }
}
?>
<!-- <-- // class Product 
// {
//     private $id;
//     private $name;
//     private $price;

//     public function __construct($i, $n, $p)
//     {
//         $this->id = $i;
//         $this->name = $n;
//         $this->price = $p;
//     }

//     public function Getid(){
//         return $this->id;
//     }

//     public function Getname(){
//         return $this->name;
//     }

//     public function Getprice(){
//         return $this->price;
//     }
// }




?> --> -->
