<?php
require 'product.php';
require 'vegetable.php';
require 'cloth.php';

// $tomorrow = new DateTime('tomorrow');
$tenDays = new DateTime('now');
$tenDays->modify('+10 day');
$twelveDays = new DateTime('now');
$twelveDays->modify('+12 day');



$vegetable1 = new Vegetable(0,'Patate',5,'Jardins du comminges',$tenDays);
$vegetable2 = new Vegetable(1,'Carotte',3,'Jardins du comminges',$tenDays);
$cloth1 = new Cloth(2,'T-Shirt',50,'guess');
$cloth2 = new Cloth(3,'Shirt',45,'lacoste');
$cloth3 = new Cloth(4,'Jean',100,'levis');
// var_dump($vegetable2).'<br>';
// var_dump($vegetable1).'<br>';
// var_dump($cloth1).'<br>';
$products = [
    $vegetable1,
    $vegetable2, 
    $cloth1, 
    $cloth2,
    $cloth3
];
// $for_vegetables = [
//     $vegetable1,
//     $vegetable2
// ];

// $for_clothes = [
//     $cloth1,
//     $cloth2,
//     $cloth3
// ];


?>