<?php


class Vegetable extends Product {
    private $productorName;
    private $expiresAt;
    public function __construct ($id,$name,$price,$productorName_i,$expiresAt_i) {
        parent::__construct($id,$name,$price);
        $this->productorName = $productorName_i;
        $this->expiresAt = $expiresAt_i;
    }
    public function get_productorName() {
        return $this->productorName;
    }
    public function get_expiresAt() {
        return $this->expiresAt;
    }
    public function isFresh(){
        // code
    }
}
?>
